Source: ghostwriter
Section: editors
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Sebastien CHAVAUX <seb95.scou@gmail.com>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-kf6,
               cmake (>= 3.16~),
               extra-cmake-modules (>= 6.0.0~),
               libcmark-gfm-dev,
               libcmark-gfm-extensions-dev,
               libhunspell-dev,
               libkf6configwidgets-dev (>= 6.0.0~),
               libkf6coreaddons-dev (>= 6.0.0~),
               libkf6doctools-dev (>= 6.0.0~),
               libkf6sonnet-dev (>= 6.0.0~),
               libkf6widgetsaddons-dev (>= 6.0.0~),
               libkf6xmlgui-dev (>= 6.0.0~),
               libxkbcommon-dev,
               pkgconf,
               qt6-base-dev (>= 6.5.0~),
               qt6-svg-dev (>= 6.5.0~),
               qt6-webchannel-dev,
               qt6-webengine-dev (>= 6.5.0~),
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://ghostwriter.kde.org/
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/ghostwriter.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/ghostwriter

Package: ghostwriter
Architecture: any
Depends: libqt6svg6, ${misc:Depends}, ${shlibs:Depends},
Suggests: cmark,
          discount,
          hunspell-dictionary,
          myspell-dictionary,
          pandoc,
          wkhtmltopdf,
Description: Distraction-free, themeable Markdown editor
 ghostwriter is a Markdown editor that provides a themable,
 distraction-free writing environment, along with a live HTML
 preview as you type, easy document navigation with an outline HUD,
 and export to popular document formats with Sundown, Pandoc,
 MultiMarkdown, Discount, cmark, or cmark-gfm processors.  It also
 features a live word count and auto-save. Eliminate distractions in
 fullscreen mode, or concentrate on the current text you are writing
 in focus mode.  It even remembers your last opened file and position
 within the file, so you can pick up where you last left off.
